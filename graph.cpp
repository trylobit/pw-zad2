#include "graph.hpp"

#include <algorithm>

namespace {

    auto greater_comp = std::greater<Edge*>();

    auto greater_pair_comp = [](
            const std::pair<Edge*, std::unique_ptr<std::atomic_bool>>& lhs,
            const std::pair<Edge*, std::unique_ptr<std::atomic_bool>>& rhs) {
        return greater_comp(lhs.first, rhs.first);
    };

};

Vertice::Vertice(id_t base_id) : base_id(base_id) {}

void Vertice::add_edge(Edge* e) {
    e->id = static_cast<unsigned int>(edges_.size());
    edges_.emplace_back(e, std::make_unique<std::atomic_bool>(false));
}

void Vertice::S_insert(Edge* edge) {
    adorators.push_back(edge);
    std::push_heap(adorators.begin(), adorators.end(), greater_comp);
    if (adorators.size() > b_limit) {
        std::pop_heap(adorators.begin(), adorators.end(),
                      greater_comp);
        adorators.pop_back();
    }
    if (adorators.size() == b_limit) {
        S_last_ = adorators.front();
    }
}

Edge* Vertice::S_last() {
    return S_last_;
}

void Vertice::T_insert(Edge* e) {
    edges_[e->id].second->store(true);
    ++adorated_counter_;
}

void Vertice::T_remove(Edge* e) {
    edges_[e->id].second->store(false);
    --adorated_counter_;
}

Edge* Vertice::find_eligible() {
    unsigned int pos = 0;
    for (auto it = edges_.begin(); it != edges_.end(); ++it, ++pos) {
        if (pos >= last_sorted_) {
            unsigned int pos_to_move = b_limit * p_value_;
            last_sorted_ += pos_to_move;
            auto it_nth = it;
            while (pos_to_move > 0 && it_nth != edges_.end()) {
                ++it_nth;
                --pos_to_move;
            }
            std::nth_element(it, it_nth, edges_.end(), greater_pair_comp);
            std::partial_sort(it, it_nth, edges_.end(), greater_pair_comp);
            unsigned int new_pos = pos;
            for (auto it_id = it; it_id != it_nth; ++it_id, ++new_pos) {
                it_id->first->id = new_pos;
            }
        }
        auto& edge = *it;
        if (!edge.second->load()) {
            if (edge.first->dest->b_limit > 0) {
                if (is_eligible(edge.first)) {
                    return edge.first;
                }
            }
        }
    }
    return nullptr;
}

bool Vertice::is_eligible(Edge* edge) {
    const Edge* last = edge->dest->S_last();
    return !last || *last < *edge->reverse;

}

void Vertice::reset(unsigned int b_limit) {
    this->b_limit = b_limit;
    exhausted = false;
    S_last_ = nullptr;
    in_next_iteration.store(false, std::memory_order_relaxed);

    adorators.clear();
    for (auto& e : edges_) {
        e.second->store(false, std::memory_order_relaxed);
    }
    adorated_counter_ = 0;
}

unsigned int Vertice::count_adorated() {
    return adorated_counter_.load();
}

Vertice::~Vertice() {
    for (auto& edge : edges_) {
        delete edge.first;
    }
}

Edge::Edge(Vertice* d, weight_t w, Edge* r) : dest(d), weight(w), reverse(r) {}

bool Edge::operator<(const Edge& other) const {
    if (weight != other.weight) {
        return weight < other.weight;
    } else {
        return dest->base_id < other.dest->base_id;
    }
}

bool Edge::operator>(const Edge& other) const {
    if (weight != other.weight) {
        return weight > other.weight;
    } else {
        return dest->base_id > other.dest->base_id;
    }
}