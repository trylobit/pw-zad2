#pragma once

#include <deque>
#include <mutex>
#include <atomic>
#include <memory>

class Vertice;

struct Edge {
    using weight_t = unsigned int;

    Vertice* dest;
    weight_t weight;
    Edge* reverse;

    Edge(Vertice*, weight_t, Edge*);

    bool operator<(const Edge&) const;

    bool operator>(const Edge&) const;

    unsigned int id{0};
};

namespace std {

    template<>
    struct less<Edge*> {
        bool operator()(Edge* const& lhs, Edge* const& rhs) const {
            return *lhs < *rhs;
        }
    };

    template<>
    struct greater<Edge*> {
        bool operator()(Edge* const& lhs, Edge* const& rhs) const {
            return *lhs > *rhs;
        }
    };

}; // std

class Vertice {
public:
    using id_t = long long;
    using adorators_container_t = std::deque<Edge*>;
private:
    using edges_container_t = std::deque<std::pair<Edge*, std::unique_ptr<std::atomic_bool>>>;

    edges_container_t edges_{};
    std::atomic<unsigned int> adorated_counter_{0};

    std::atomic<Edge*> S_last_{nullptr};

    constexpr static unsigned int p_value_ = 7;
    unsigned int last_sorted_{0};

    friend class Edge;

public:
    const id_t base_id;

    std::mutex mtx{};

    adorators_container_t adorators{};

    bool exhausted = false;

    unsigned int b_limit = 0;

    std::atomic_bool in_next_iteration{false};

    explicit Vertice(id_t base_id);

    void add_edge(Edge*);

    void S_insert(Edge*);

    Edge* S_last();

    void T_insert(Edge*);

    void T_remove(Edge*);

    Edge* find_eligible();

    bool is_eligible(Edge*);

    void reset(unsigned int b_limit);

    unsigned int count_adorated();

    ~Vertice();
};