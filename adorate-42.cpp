#include "blimit.hpp"
#include "graph.hpp"

#include <iostream>
#include <fstream>
#include <map>
#include <deque>
#include <sstream>
#include <mutex>
#include <atomic>
#include <thread>

void load_graph(const std::string& filename, std::deque<Vertice>& graph) {
    graph.clear();

    std::string line, input_number;
    std::stringstream line_stream;
    Vertice::id_t from, to;
    Edge::weight_t weight;

    std::ifstream file(filename);

    using map = std::map<Vertice::id_t, unsigned long long>;
    map used_ids{};

    while (getline(file, line)) {
        if (line[0] != '#') {
            line_stream = std::stringstream(line);
            line_stream >> from >> to >> weight;

            if (used_ids.count(from) == 0) {
                auto new_from = graph.size();
                graph.emplace_back(from);
                used_ids[from] = new_from;
                from = new_from;
            } else {
                from = used_ids[from];
            }

            if (used_ids.count(to) == 0) {
                auto new_to = graph.size();
                graph.emplace_back(to);
                used_ids[to] = new_to;
                to = new_to;
            } else {
                to = used_ids[to];
            }

            Edge* first = new Edge(&graph[to], weight, nullptr);
            Edge* second = new Edge(&graph[from], weight, first);
            first->reverse = second;

            graph[from].add_edge(first);
            graph[to].add_edge(second);
        }
    }
}

int main(int argc, char* argv[]) {

    if (argc != 4) {
        std::cerr << "usage: " << argv[0] << " thread-count inputfile b-limit"
                  << std::endl;
        return 1;
    }

    unsigned int thread_count = static_cast<unsigned int>(std::stoi(argv[1]));
    int b_limit = std::stoi(argv[3]);
    std::string input_filename{argv[2]};

    std::deque<Vertice> graph{};
    load_graph(input_filename, graph);

    std::deque<std::thread> threads{};

    std::deque<Vertice*> Q(graph.size());
    std::deque<Vertice*> Q_prim(graph.size());

    std::atomic<unsigned int> next_Q_pos{0};
    unsigned long Q_size{0};
    std::atomic<unsigned int> next_Q_prim_pos{0};

    std::atomic<unsigned long long> result{0};

    auto reset = [&](int b_method) {
        unsigned int my_pos = next_Q_pos++;
        while (my_pos < Q_size) {
            Q[my_pos] = &graph[my_pos];
            graph[my_pos].reset(bvalue(static_cast<unsigned int>(b_method),
                                       static_cast<unsigned long>(graph[my_pos].base_id)));
            my_pos = next_Q_pos++;
        }
    };

    auto compute = [&]() {
        unsigned int my_pos = next_Q_pos++;
        while (my_pos < Q_size) {
            Vertice* from = Q[my_pos];
            while (from->count_adorated() <
                   from->b_limit && !from->exhausted) {
                Edge* p = from->find_eligible();
                if (p) {
                    Edge* new_edge = p;
                    {
                        std::lock_guard<std::mutex> lck(
                                new_edge->dest->mtx);
                        if (from->is_eligible(new_edge)) {
                            Edge* outdated_edge = new_edge->dest->S_last();
                            new_edge->dest->S_insert(new_edge->reverse);
                            from->T_insert(new_edge);
                            if (outdated_edge) {
                                outdated_edge->dest->T_remove(
                                        outdated_edge->reverse);
                                Vertice* outdated_vertice = outdated_edge->dest;
                                if (!outdated_vertice->in_next_iteration.exchange(
                                        true)) {
                                    Q_prim[next_Q_prim_pos++] = outdated_vertice;
                                }
                            }
                        }
                    }
                } else {
                    from->exhausted = true;
                }
            }

            my_pos = next_Q_pos++;
        }
    };

    auto sum = [&]() {
        unsigned int my_pos = next_Q_pos++;
        unsigned long long my_result = 0;
        while (my_pos < Q_size) {
            for (const auto& edge_ptr : graph[my_pos].adorators) {
                my_result += edge_ptr->weight;
            }

            my_pos = next_Q_pos++;
        }
        result += my_result;
    };

    for (int b_method = 0; b_method < b_limit + 1; b_method++) {
        //region init

        Q_size = graph.size();

        for (unsigned int i = 0; i < thread_count - 1; ++i) {
            threads.emplace_back(reset, b_method);
        }
        reset(b_method);
        for (unsigned int i = 0; i < thread_count - 1; ++i) {
            threads[i].join();
        }
        threads.clear();
        next_Q_pos = 0;

        //endregion

        //region computatons

        while (Q_size != 0) {
            for (unsigned int i = 0; i < thread_count - 1; ++i) {
                threads.emplace_back(compute);
            }
            compute();
            for (unsigned int i = 0; i < thread_count - 1; ++i) {
                threads[i].join();
            }
            threads.clear();

            {
                unsigned int Q_pos;
                for (Q_pos = 0;
                     Q_pos < next_Q_prim_pos.load(); ++Q_pos) {
                    Q_prim[Q_pos]->in_next_iteration.store(false,
                                                       std::memory_order_relaxed);
                    Q[Q_pos] = Q_prim[Q_pos];
                }
                Q_size = Q_pos;
            }
            next_Q_pos = 0;
            next_Q_prim_pos = 0;
        }

        //endregion

        //region result

        Q_size = graph.size();
        result = 0;

        for (unsigned int i = 0; i < thread_count - 1; ++i) {
            threads.emplace_back(sum);
        }
        sum();
        for (unsigned int i = 0; i < thread_count - 1; ++i) {
            threads[i].join();
        }
        threads.clear();
        next_Q_pos = 0;

        std::cout << result / 2 << std::endl;

        //endregion
    }
}
