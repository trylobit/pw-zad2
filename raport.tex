\documentclass[12pt,a4paper,oneside]{article}

\title{\emph{b}-Suitor algorithm implementation report}
\date{2017-12-31}
\author{Norbert Siwek}

\usepackage{hyperref}
\usepackage{enumitem}
\usepackage[T1]{fontenc}

\fontfamily{lmr}\selectfont

\begin{document}

% === INTRO === 

\pagenumbering{gobble}

\maketitle
\newpage

\pagenumbering{arabic}

\tableofcontents
\newpage

% === CONTENT === 

\section{Introduction}

I have implemented parallel \emph{b}-Suitor Algorithm from \href{https://www.cs.purdue.edu/homes/apothen/Papers/bMatching-SISC-2016.pdf}{here}. My solution consists of following files:
\begin{description}
\item[adorate-42.cpp] Main file.
\item[graph.hpp] Header for Edge and Vertice classes.
\item[graph.cpp] Implementation of graph.hpp.
\item[blimit.hpp] Header containg bvalue function.
\item[blimit.cpp] Implementation of bvalue function.
\end{description}

\newpage

\section{Implementation details}
For all sections below I consider graph $G=(V,E)$ as input graph, where $V$ is a set of vertices and $E$ is a set of nodes. I denote $n=|V|$ and $m=|E|$. 

\subsection{Implementation decisions}
Some of class members I use are not copyable (e.g. \emph{std::mutex}), so to avoid being forced to use opaque structures, which would result in more convoluted code, I resort to using \emph{std::deque} insted of \emph{std::vector} for dynamic arrays. Thus, combined with calling \emph{emplace} functions instead of their \emph{push} counterparts, I am able to define non-copyable class Vertice. I've found that code is clearer, when I store two directed edges with pointers to each others. Every edge and vertice is stored in dynamic memory. Vertices are managed by \emph{std::deque} storage system. Edges are allocated manually and each vertice is responsible for freeing all the edges coming out of it at destruction. Comparison of edges is done either using member comparison operators or with \emph{std::greater} and \emph{std::less} specializations I've provided.

\subsection{Main file}

\begin{description}
\item[load\_graph function] \hfill \\
This function loads graph from input file using std::ifstream. It maps vertices base ids to their indexes in graph array. I need to save the base vertice id for comparisons. Complexity of \emph{Vertice::add\_edge(const Edge*)} is $O(1)$. It results in $O(m\log n)$ complexity of loading graph.
\item[main function] \hfill \\
Begins with parsing input and loading graph, then come the declarations and initializations of folowing structures:
\begin{description}
\item[threads array] Used to keep track of created threads.
\item[Q] Set of vertices processed in current iteration (represented by an array).
\item[Q\_prim] Queue of vertices to be processed in next iteration (represented by an array).
\item[Q\_size] Variable holding current \emph{Q.size()}.
\item[next\_Q\_pos, next\_Q\_prim\_pos] Atomic variables used to synchronize adding and reading from corresponding arrays.
\item[result] Variable used to calculate result of algorithm.
\item[reset] Function which I run to reset vertices to their base states before every bmethod computation. I guarantee its parallel safety by using synchronized queue, which is implemented using atomic variable \emph{next\_Q\_pos}, which points to the head of queue. In my implementation each vertices' reset function is processed independently.
\item[compute] Function which implements \emph{b}-Suitor algorithm. It processes all vertices which are currently in \emph{Q} queue. I guarantee its parallel safety by using synchronized queue, which is implemented using atomic variable \emph{next\_Q\_pos}, which points to the head of queue. I use mutex as specified in \emph{b}-Suitor alogorithm. I additionally specify that both \emph{Q} holds each vertice only once, which gives me invariant that no two threads will process the same vertice in main function loop at the same time.
\item[sum] Function which I run to calculate the result of algorithm. I guarantee its parallel safety by using synchronized queue, which is implemented using atomic variable \emph{next\_Q\_pos}, which points to the head of queue.
\end{description}

The main loop iterates over \emph{b\_methods} to compute. First it runs \emph{reset} function.\emph{Vertice::reset(unsigned int)} complexity is $O(\alpha)$, where $\alpha$ is the number of given vertice neighbours, so entire reseting part takes $O(n + m)$ time. Then I run compute function in loop to process vertices as in \emph{b}-Suitor algorithm. After execution I calculate the result by adding the weights of edges in adorators priority queues and divide the result by two (I store two directed edges instead of one, so both would be include in sum). Calculating the result takes $O(\sum_{v\in V} v.b\_limit)$ time.
\end{description}

\subsection{Edge class}

Edge class member variables: 
\begin{description}
\item[dest] Pointer to vertice that edge points to.
\item[weight] Weight of given edge.
\item[reverse] Pointer to reverse edge.
\item[id] Index of this edge in its beginning vertice \emph{N} set.
\item[operator<, >] Operators that describe linear order between edges, as mentioned in \emph{b}-Suitor algorithm.
\end{description}

\subsection{Vertice class}

Vertice class member variables (for this subsection I denote $\alpha$ to be equal to number of edges coming from this vertice):
\begin{description}
\item[edges\_] Array of edges with their flags. Throughout the execution I maintain the property that flag of edge $e$ is set to $true$ if and only if this vertice adorates vertice \emph{e->dest}. This array corresponds to $N$ and $T$ sets from \emph{b}-Suitor algorithm. I maintain the property that \emph{Edge::id}$\equiv$\emph{Edge index in edges array} before and after each \emph{find\_eligible} function call.
\item[adorated\_counter\_] Counter of vertices that this vertice adorates.
\item[S\_last\_] Variable containing return value of \emph{S.last()} function. 
\item[p\_value\_] \emph{p(v)} as in \emph{b}-Suitor algorihm ST DP.
\item[last\_sorted\_] index of last sorted edge
\item[base\_id] Base id of this vertice.
\item[mtx] Mutex used to perform \emph{Lock p} operation from \emph{b}-Suitor algorithm.
\item[adorators] Priority queue corresponding to \emph{S} priority queue from \emph{b}-Suitor algorithm.
\item[exhausted] Flag for marking this vertice's neighbours set as exhausted.
\item[b\_limit] Variable containing current \emph{b\_limit} of this vertice.
\item[in\_next\_iteration] Flag which indicates if this vertice was added to the \emph{Q\_prim} queue of vertices to be processed in next iteration of \emph{compute} function in \emph{main}.
\item[Vertice constructor] Constructs new vertice. $O(1)$.
\item[add\_edge] Function used to add edge to this vertice. It initializes \emph{Edge::id} and its flag. $O(1)$.
\item[S\_insert] Inserts new edge to \emph{adorators} priority queue and then updates \emph{S\_last\_}. Updating priority queue takes $O(b\_limit)$ time.
\item[S\_last] Returns \emph{S\_last\_}. $O(1)$.
\item[T\_insert, T\_remove] Perform corresponding operations from \emph{b}-Suitor algorithm. $O(1)$.
\item[find\_eligible] Finds best eligible partner for this vertice. It iterates over \emph{edges\_} array. If requied, it partially sorts \emph{edges\_} array and updates \emph{Edge::id} of sorted edges. Without sorting: $O(\alpha)$. When sorting is required $O(\alpha \log \alpha)$.
\item[is\_eligible] Checks if edge connects to eligible partner for this vertice. $O(1)$.
\item[reset] Function which resets vertice state. It is used in \emph{reset} function in \emph{main}. $O(\alpha)$.
\item[count\_adorated] Function which gives access to \emph{adorated\_counter\_} variable. $O(1)$.
\item[Vertice destructor] Frees edges from \emph{edges\_} container. $O(\alpha)$.
\end{description}

\newpage

\section{Optimizations}

I reduce number of used mutexes to minimum, resorting to using \emph{std::atomic} instead. I process \emph{reset}, \emph{compute} and \emph{sum} functions in parallel, running \emph{thread\_count} threads each time. In \emph{compute} function I use a flag to specify if a vertice should be processed in next iteration, which prevents vertices from being added to \emph{Q\_prim} twice.

\section{Tests}

\subsection{Test method}
I have been testing on modifief version of skitter graph from \href{http://snap.stanford.edu/data/}{here} on students. I've been measuring time of algorithm using \emph{time} command. When launched with $b\_limit = -1$ my program only loads grap, initialized all the structures and then destroys it. So by substracting this time from overall time given by \emph{time} command I get the time, which took my program to compute its input.

\begin{center}
\begin{tabular}{|c|l|}
\hline
\textbf{Vertices} & 937099\\
\hline
\textbf{Edges} & 5841835\\
\hline
\textbf{B limit} & 20\\
\hline
\end{tabular}
\end{center}

\subsection{Results}
Measured input time: 0m53.582s\\
Times below do not include input time.
\begin{center}
\begin{tabular}{|c|l|c|}
\hline
\textbf{Thread count} & \textbf{Time} & \textbf{Speedup}\\
\hline
1 & 1m19.765s & 1\\
\hline
2 & 1m14.344s & 1.07\\
\hline
3 & 0m56.890s & 1.40\\
\hline
4 & 0m50.537s & 1.58\\
\hline
5 & 0m44.515s & 1.79\\
\hline
6 & 0m44.896s & 1.77\\
\hline
7 & 0m31.633s & 2.52\\
\hline
8 & 0m28.900s & 2.76\\
\hline
\end{tabular}
\end{center}

\subsection{Commentary}
The higher the thread count, the higher the speedup. Irregularities observed in speedup can be caused by different orders of processing, as stated in \href{https://www.cs.purdue.edu/homes/apothen/Papers/bMatching-SISC-2016.pdf}{here}. Additionally students machine is not an isolated environment and performance is affected by other tasks it has to process.

\end{document}